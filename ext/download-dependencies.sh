#!/bin/bash
set -e

# change to the directory where the script is stored
cd `dirname $0`

# Download all the external dependencies. They are then built using
# CMake's ExternalProject module. The module could download the
# dependencies too, but it needs e.g. mercurial to get the SDL source
# and mercurial is not available in the Ubuntu SDK containers by default.
# So I created this shell script. Run it once, outside of the chroot.
# You still need mercurial for this.

if [ -e SDL2 ]; then
	echo "Skipping SDL2 because it's been downloaded already."
else
	# SDL2 version 2.0.5 seems to still work OK, while 2.0.6
	# fails to start. So stick with 2.0.5 for now.
	echo "Downloading SDL2..."
	hg clone -r release-2.0.5 http://hg.libsdl.org/SDL SDL2
fi

if [ -e SDL2_image ]; then
	echo "Skipping SDL2_image because it's been downloaded already."
else
	# SDL2_image versions > 2.0.2 require SDL2 version >= 2.0.8. Furthermore,
	# SDL2_image 2.0.2 fails to build (some PNG issues?)... so just stick
	# with the old and working SDL2_image 2.0.1.
	echo "Downloading SDL2_image..."
	hg clone -r release-2.0.1 https://hg.libsdl.org/SDL_image SDL2_image
fi

if [ -e SDL2_ttf ]; then
	echo "Skipping SDL2_image because it's been downloaded already."
else
	# SDL2_ttf versions > 2.0.14 require SDL2 version >= 2.0.8, so
	# stick with SDL2_ttf 2.0.14.
	echo "Downloading SDL2_ttf..."
	hg clone -r release-2.0.14 http://hg.libsdl.org/SDL_ttf/ SDL2_ttf
fi

if [ -e SDL2_mixer ]; then
	echo "Skipping SDL2_mixer because it's been downloaded already."
else
	echo "Downloading SDL2_mixer..."
	# SDL2_mixer versions > 2.0.1 require SDL2 version >= 2.0.7, so
	# stick with SDL2_mixer 2.0.1.
	hg clone -r release-2.0.1 http://hg.libsdl.org/SDL_mixer/ SDL2_mixer
fi

if [ -e SDL2pp ]; then
	echo "Skipping SDL2pp because it's been downloaded already."
else
	echo "Downloading SDL2pp..."

	# shallow clone tag 0.16.0
	git clone --depth 1 --branch 0.16.0 https://github.com/libSDL2pp/libSDL2pp.git SDL2pp
fi
